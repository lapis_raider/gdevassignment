#include "BryanQT.h"
#include "stdio.h"
#include "Collider\Collider.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"

vector<BryanQTGrid*> BryanQT::m_gridList;

BryanQT::BryanQT()
{
	// Pre-define 100 grids so I dont new every time i get a new grid
	m_gridList.resize(100);
	for (int i = 0; i < (int)m_gridList.size(); ++i)
		m_gridList[i] = new BryanQTGrid();
}

BryanQT::~BryanQT()
{
	if (root)
	{
		delete root;
		root = NULL;
	}
	m_gridList.clear();
}

void BryanQT::Destroy()
{
	if (root)
	{
		delete root;
		root = NULL;
	}
	m_gridList.clear();
	Singleton<BryanQT>::Destroy();
}

void BryanQT::Init(Vector3 origin, float width, float height)
{
	root = FetchGrid(origin, width, height, 0, NULL);
}

void BryanQT::Add(EntityBase* entity)
{
	root->Insert(entity);
}

void BryanQT::Update()
{
	root->Update();
}

BryanQTGrid* BryanQT::FetchGrid(Vector3 origin, float width, float height, int depth, BryanQTGrid* parent)
{
	for (auto grid : m_gridList)
	{
		if (grid->active)
			continue;

		// Set grid essential data
		grid->m_origin = origin;
		grid->m_width = width;
		grid->m_height = height;
		grid->active = true;
		grid->ClearElement();
		grid->m_parent = parent;
		grid->depth = depth;
		grid->objCount = 0;
		grid->partition = false;

		return grid;
	}

	// Pre-define another 10 grids for use
	for (int i = 0; i <= 10; ++i)
	{
		m_gridList.push_back(new BryanQTGrid());
	}
	return FetchGrid(origin, width, height, depth, parent);
}

vector<EntityBase*> BryanQT::GetEntities(Vector3 point, float radius)
{
	if (root)
		return root->GetEntities(point, radius);
}

bool BryanQT::Remove(EntityBase* entity)
{
	return root->Remove(entity);
}

void BryanQT::SetMeshRenderMode(BryanQTGrid::SMeshRenderMode meshRenderMode)
{
	this->meshRenderMode = meshRenderMode;
}

void BryanQT::Render(Vector3* theCameraPosition, Vector3* theCameraDirection)
{
	if (root)
		root->Render();
}

void BryanQT::SetMesh(const std::string& _meshName)
{
	this->_meshName = _meshName;
	root->SetMesh(_meshName);
}

BryanQTGrid::SMeshRenderMode BryanQT::GetMeshRenderMode(void) const
{
	return BryanQTGrid::FILL;
}
