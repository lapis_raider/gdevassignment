#pragma once

#include "EntityBase.h"
#include "Vector3.h"
#include "Mesh.h"
#include <vector>
using namespace std;

#include "../LevelOfDetails/LevelOfDetails.h"
#include <list>

//Include GLEW
#include <GL/glew.h>

class BryanQTGrid
{
public:
	enum SMeshRenderMode
	{
		WIRE,
		FILL,
		NUM_MODE
	};
	enum GRID_NAME
	{
		NW = 0, // NorthWest Grid
		NE, // NorthEast Grid
		SW, // SouthWest Grid
		SE, // SouthEast Grid
		TOTAL_GRID, // Total Grid
	};

	BryanQTGrid();
	BryanQTGrid(Vector3 origin, float width, float height, int depth = 0, BryanQTGrid* parent = NULL);
	~BryanQTGrid();

	// Set Grid's Mesh
	void SetMesh(const std::string& _meshName);
	// Set Mesh's Render Mode
	void SetMeshRenderMode(BryanQTGrid::SMeshRenderMode meshRenderMode = FILL);
	// Get Mesh's Render Mode
	BryanQTGrid::SMeshRenderMode GetMeshRenderMode(void) const;

	// Grid methods
	bool IsInGrid(EntityBase* entity); // Check if entity is in grid
	bool IsInGrid(Vector3 point, float radius); // Check if grid is in range of point (AABB)
	bool IsInGrid(Vector3 point, float radius, EntityBase* entity); // Check if entitiy is in range point (Distance)
	bool Insert(EntityBase* entity);
	bool ReInsert(BryanQTGrid* grid, EntityBase* entity);
	void Partition();
	void Unpartition();
	void RemoveGrids();
	vector<EntityBase*> GetEntities(Vector3 point, float radius);
	bool Remove(EntityBase* entity);

	void Render(void);
	void Update(void);

	list<EntityBase*> GetElement();
	void ClearElement();
	bool active; // If grid is active

	Vector3 m_origin; // Center of grid
	float m_width; // Width of grid
	float m_height; // Height of grid
	int depth; // depth
	int objCount;
	bool partition;

	BryanQTGrid* m_parent; // Pointer to parent grid
private:
	list<EntityBase*> m_element; // Element obj in grid

	vector<BryanQTGrid*> m_grids; // Vector of grid pointers

	// The mesh to represent the grid
	Mesh* theMesh;
	// Define the mesh render mode
	SMeshRenderMode meshRenderMode;
	std::string meshName;
};