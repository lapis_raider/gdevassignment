#include "BryanQTGrid.h"
#include "stdio.h"
#include "MeshBuilder.h"
#include "RenderHelper.h"
#include "../GenericEntity.h"
#include "../SceneGraph/SceneGraph.h"
#include "GraphicsManager.h"
#include <algorithm>

#include "BryanQT.h" // this is bad but oh well

BryanQTGrid::BryanQTGrid()
{
	m_origin.SetZero();
	theMesh = NULL;
	meshName = "";
	active = false;
	m_element.clear();
	m_parent = NULL;
	depth = 0;
	objCount = 0;
	partition = false;
}

BryanQTGrid::BryanQTGrid(Vector3 origin, float width, float height, int depth, BryanQTGrid* parent)
{
	m_origin = origin;
	m_width = width;
	m_height = height;
	m_parent = parent;
	this->depth = depth;
	theMesh = NULL;
	meshName = "";
	active = false;
	m_element.clear();
	objCount = 0;
	partition = false;
}

BryanQTGrid::~BryanQTGrid()
{
	m_grids.clear();
	m_element.clear();
	delete m_parent;
	m_parent = NULL;
}

void BryanQTGrid::SetMesh(const std::string& _meshName)
{
	meshName = _meshName;
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh)
		theMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
}

void BryanQTGrid::SetMeshRenderMode(SMeshRenderMode meshRenderMode)
{
	this->meshRenderMode = meshRenderMode;
}

BryanQTGrid::SMeshRenderMode BryanQTGrid::GetMeshRenderMode(void) const
{
	return meshRenderMode;
}

void BryanQTGrid::Render(void)
{
	if (!active)
		return;

	if (!partition) // Render if not partitioned
	{
		MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

		modelStack.PushMatrix();
		modelStack.Translate(m_origin.x, m_origin.y, m_origin.z);
		modelStack.Rotate(-90, 1, 0, 0);
		modelStack.Scale(m_width, m_height, 1.0f);
		RenderHelper::RenderMesh(theMesh);
		modelStack.PopMatrix();
	}
	else
	{
		for (auto grid : m_grids)
		{
			grid->Render();
		}
	}
}

void BryanQTGrid::Update(void)
{
	if (partition)
	{
		for (auto grid : m_grids)
		{
			grid->Update();
		}
	}
	else
	{
		if (m_element.empty())
			return;

		vector<EntityBase*> migrationList;
		for (auto i : m_element)
		{
			if (!IsInGrid(i)) // If any element is no longer inside of grid
			{
				migrationList.push_back(i);
			}
		}

		for (auto i : migrationList)
		{
			m_element.remove(i);
			--objCount;
			ReInsert(m_parent, i);
		}
	}
}

bool BryanQTGrid::ReInsert(BryanQTGrid* grid, EntityBase* entity)
{
	if (!entity)
		return false;

	if (grid->IsInGrid(entity))
	{
		if (grid->Insert(entity))
		{
			--grid->objCount;
			return true;
		}
	}
	else
	{
		ReInsert(grid->m_parent, entity);
		--grid->objCount;
		grid->Unpartition();
	}

	return false;
}

void BryanQTGrid::Partition()
{
	float quaterWidth = m_width / 4;
	float quaterHeight = m_height / 4;
	float halfWidth = m_width / 2;
	float halfHeight = m_height / 2;

	// NW grid
	m_grids.push_back(BryanQT::FetchGrid(Vector3(m_origin.x - quaterWidth, m_origin.y, m_origin.z + quaterHeight), halfWidth, halfHeight, depth + 1, this));
	// NE grid														
	m_grids.push_back(BryanQT::FetchGrid(Vector3(m_origin.x + quaterWidth, m_origin.y, m_origin.z + quaterHeight), halfWidth, halfHeight, depth + 1, this));
	// SW grid																														
	m_grids.push_back(BryanQT::FetchGrid(Vector3(m_origin.x - quaterWidth, m_origin.y, m_origin.z - quaterHeight), halfWidth, halfHeight, depth + 1, this));
	// SE grid																															
	m_grids.push_back(BryanQT::FetchGrid(Vector3(m_origin.x + quaterWidth, m_origin.y, m_origin.z - quaterHeight), halfWidth, halfHeight, depth + 1, this));

	partition = true;

	for (auto grid : m_grids)
		grid->SetMesh(meshName);
}

void BryanQTGrid::Unpartition()
{
	if (!partition) // if not partitioned dont run
		return;

	if (objCount > 1)
		return;

	for (auto grid : m_grids)
	{
		// Re-assign child element into parent element
		if (!grid->GetElement().empty())
		{
			for (auto i : grid->GetElement())
			{
				m_element.push_back(i);
			}
			break;
		}
	}

	RemoveGrids();

	partition = false;
}

void BryanQTGrid::RemoveGrids()
{
	if (partition)
	{
		for (auto grid : m_grids)
		{
			grid->RemoveGrids();
			grid->active = false;
			grid = NULL;
		}
		partition = false;
		m_grids.clear();
	}
	else
	{
		theMesh = NULL;
		objCount = 0;
		m_element.clear();
	}
}

vector<EntityBase*> BryanQTGrid::GetEntities(Vector3 point, float radius)
{
	vector<EntityBase*> entityList;

	if (!IsInGrid(point, radius))
		return entityList;
	else if (!m_element.empty())
	{
		for (auto i : m_element)
		{
			if (IsInGrid(point, radius, i))
			{
				entityList.push_back(i);
			}
		}
	}

	if (partition)
	{
		for (auto grid : m_grids)
		{
			vector<EntityBase*> tempList;
			tempList = grid->GetEntities(point, radius);
			entityList.insert(entityList.end(), tempList.begin(), tempList.end());
		}
	}

	return entityList;
}

bool BryanQTGrid::Remove(EntityBase* entity)
{
	// If entity doesn't belong in grid
	if (!IsInGrid(entity))
		return false;

	--objCount;

	// if partitioned
	if (partition)
	{
		for (auto grid : m_grids)
		{
			if (grid->Remove(entity))
				break;
		}
		Unpartition();
		return true;
	}
	else
	{
		m_element.clear();
		return true;
	}
}

bool BryanQTGrid::IsInGrid(EntityBase* entity)
{
	Vector3 tempPos = entity->GetPosition();

	return (tempPos.x > m_origin.x - m_width / 2 &&
		tempPos.x < m_origin.x + m_width / 2 &&
		tempPos.z > m_origin.z - m_height / 2 &&
		tempPos.z < m_origin.z + m_height / 2);
}

bool BryanQTGrid::IsInGrid(Vector3 point, float radius)
{
	return !(point.x - radius > m_origin.x + m_width ||
		point.x + radius < m_origin.x - m_width ||
		point.z - radius > m_origin.z + m_height ||
		point.z + radius < m_origin.z - m_height);
}

bool BryanQTGrid::IsInGrid(Vector3 point, float radius, EntityBase* entity)
{
	return (Vector3(abs(point.x - entity->GetPosition().x), 0, abs(point.z - entity->GetPosition().z)).LengthSquared() < radius * radius);
}

bool BryanQTGrid::Insert(EntityBase* entity)
{
	if (!entity)
		return false;

	if (!IsInGrid(entity))
		return false;

	++objCount;

	if (objCount <= 4)
	{
		m_element.push_back(entity);
		return true;
	}
	else if (!partition) // If not partitioned yet
		Partition();

	for (auto grid : m_grids)
	{
		for (auto i : m_element)
		{
			if (grid->Insert(i))
			{
				m_element.remove(i);
				break;
			}
		}
	}

	for (auto grid : m_grids)
	{
		if (grid->Insert(entity))
			return true;
	}

	return false;
}

list<EntityBase*> BryanQTGrid::GetElement()
{
	if (!m_element.empty())
	{
		if (partition)
		{
			for (auto grid : m_grids)
			{
				if (!grid->GetElement().empty())
					return grid->GetElement();
			}
		}
		else
			return list<EntityBase*>();

		return list<EntityBase*>();
	}

	return m_element;
}

void BryanQTGrid::ClearElement()
{
	m_element.clear();
}
