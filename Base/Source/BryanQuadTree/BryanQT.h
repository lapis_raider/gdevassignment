#pragma once
#include "Vector3.h"
#include "BryanQTGrid.h"
#include "EntityBase.h"
#include "SingletonTemplate.h"
#include "../FPSCamera.h"

class BryanQT : public Singleton<BryanQT>
{
private:
	friend Singleton<BryanQT>;

	BryanQT();

	static vector<BryanQTGrid*> m_gridList;
	BryanQTGrid* root;

	// Mesh Stuff
	std::string _meshName; // Name of the mesh
	std::string _meshName2; // Name of the mesh
	std::string _meshName3; // Name of the mesh
	std::string _meshName4; // Name of the mesh
	std::string _meshName5; // Name of the mesh
	BryanQTGrid::SMeshRenderMode meshRenderMode; // Mesh Mode
public:
	virtual ~BryanQT();
	void Destroy();

	void Init(Vector3 origin, float width, float height);
	void Add(EntityBase* entity);
	void Update();
	static BryanQTGrid* FetchGrid(Vector3 origin, float width, float height, int depth, BryanQTGrid* parent);
	vector<EntityBase*> GetEntities(Vector3 point, float radius);
	bool Remove(EntityBase* entity);

	// Mesh stuff
	void BryanQT::SetMeshRenderMode(BryanQTGrid::SMeshRenderMode meshRenderMode);
	void BryanQT::Render(Vector3* theCameraPosition = NULL, Vector3* theCameraDirection = NULL);
	void BryanQT::SetMesh(const std::string& _meshName);
	BryanQTGrid::SMeshRenderMode BryanQT::GetMeshRenderMode(void) const;
};

