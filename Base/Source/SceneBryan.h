#ifndef SCENE_TEXT_H
#define SCENE_TEXT_H

#include "Scene.h"
#include "Mtx44.h"
#include "PlayerInfo/PlayerInfo.h"
#include "GroundEntity.h"
#include "FPSCamera.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "GenericEntity.h"
#include "Enemy/Enemy3D.h"
#include "HardwareAbstraction\Keyboard.h"
#include "Minimap\Minimap.h"
#include "CameraEffects\CameraEffects.h"
#include "HardwareAbstraction\Mouse.h"

class ShaderProgram;
class SceneManager;
class TextEntity;
class Light;
class SceneBryan : public Scene
{
public:
	SceneBryan();
	~SceneBryan();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

protected:
	// Create Entities to display in this game
	virtual void CreateEntities(void);
	void CreateTree(Vector3 pos);

private:
	SceneBryan(SceneManager* _sceneMgr); // This is used to register to SceneManager

	ShaderProgram* currProg;
	CPlayerInfo* playerInfo;
	GroundEntity* groundEntity;
	FPSCamera camera;
	bool swapCamera;
	FPSCamera birdEye;
	ostringstream DisplayText;
	TextEntity* textObj[3];
	Light* lights[2];

	CKeyboard* theKeyboard;
	CMouse* theMouse;
	CMinimap* theMinimap;
	CCameraEffects* theCameraEffects;

	GenericEntity* theCube;
	//	CEnemy3D* anEnemy3D;	// This is the CEnemy class for 3D use.

	static SceneBryan* sInstance; // The pointer to the object that gets registered
};

#endif