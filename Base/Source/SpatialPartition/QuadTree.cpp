#include "QuadTree.h"
#include "stdio.h"
#include "Collider\Collider.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"


QuadTree::QuadTree(void)
{

}

QuadTree::~QuadTree(void)
{
	if (root)
	{
		delete root;
		root = NULL;
	}
}

void QuadTree::Destroy()
{
	Singleton<QuadTree>::Destroy();
}

void QuadTree::Init(const Vector3 originPos,
	const float gridWidth, const float gridHeight)
{
	this->origin = originPos;
	startingPos = Vector3(originPos.x - gridWidth / 2, originPos.y ,originPos.z + gridHeight / 2); //top left pos

	root = new QuadTreeGrid(startingPos, gridWidth, gridHeight, root); //initialize the root tree details
}

void QuadTree::Add(EntityBase* theObject)
{
	root->Insert(theObject);
}

void QuadTree::Update()
{
	root->Update();
}


void QuadTree::SetMeshRenderMode(QuadTreeGrid::SMeshRenderMode meshRenderMode)
{
	this->meshRenderMode = meshRenderMode;
}

void QuadTree::Render(Vector3* theCameraPosition, Vector3* theCameraDirection)
{
	// Render the grids
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	if (root)
		root->Render();
}

vector<EntityBase*> QuadTree::ObjsInRange(Vector3 pos, float range)
{
	return root->FindObjectsInRange(pos, range);
}

void QuadTree::Remove(EntityBase* obj)
{
	root->DeleteObjects(obj);
}

void QuadTree::SetMesh(const std::string& _meshName)
{
	this->_meshName = _meshName;
	root->SetMesh(_meshName);
}

QuadTreeGrid::SMeshRenderMode QuadTree::GetMeshRenderMode(void) const
{
	return QuadTreeGrid::FILL;
}


void QuadTree::SetLevelOfDetails(const float distance_High2Mid, const float distance_Mid2Low)
{

}