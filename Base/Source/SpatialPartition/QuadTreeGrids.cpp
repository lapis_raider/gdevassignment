#include "QuadTreeGrids.h"
#include "stdio.h"
#include "MeshBuilder.h"
#include "RenderHelper.h"
#include "../GenericEntity.h"
#include "../SceneGraph/SceneGraph.h"
#include "GraphicsManager.h"

QuadTreeGrid::QuadTreeGrid()
{
	obj = NULL;
	divded = false;
	activeGrid = false;

	topRight = NULL;
	topLeft = NULL;
	bottomRight = NULL;
	bottomLeft = NULL;
	parentGrid = NULL;
}

QuadTreeGrid::QuadTreeGrid(Vector3 startingPos, float gridWidth, float gridHeight, QuadTreeGrid* parent)
{
	this->startingPos = startingPos;
	this->gridWidth = gridWidth;
	this->gridHeight = gridHeight;
	this->orginPt = Vector3(startingPos.x + gridWidth /2, 0.f,startingPos.z - gridHeight / 2);

	obj = NULL;
	divded = false;

	this->parentGrid = parent;
	topRight = NULL;
	topLeft = NULL;
	bottomRight = NULL;
	bottomLeft = NULL;

	objNumber = 0;
}

void QuadTreeGrid::Init(Vector3 startingPos, float gridWidth, float gridHeight, QuadTreeGrid* parent)
{
	this->startingPos = startingPos;
	this->gridWidth = gridWidth;
	this->gridHeight = gridHeight;
	this->orginPt = Vector3(startingPos.x + gridWidth / 2, 0.f, startingPos.z - gridHeight / 2);

	obj = NULL;
	divded = false;
	activeGrid = true;

	this->parentGrid = parent;
	topRight = NULL;
	topLeft = NULL;
	bottomRight = NULL;
	bottomLeft = NULL;

	objNumber = 0;
}

QuadTreeGrid::~QuadTreeGrid()
{
	if (divded) //make sure its children is empty first
	{
		topRight->Destory();
		topLeft->Destory();
		bottomRight->Destory();
		bottomLeft->Destory();
	}
	else //if its the children
	{
		theMesh = NULL;
		objNumber = 0;
		obj = NULL;
	}

	divded = false;
	delete topRight;
	topRight = NULL;

	delete topLeft;
	topLeft = NULL;

	delete bottomRight;
	bottomRight = NULL;

	delete bottomLeft;
	bottomLeft = NULL;
}

void QuadTreeGrid::Destory()
{
	if (divded) //make sure its children is empty first
	{
		topRight->Destory();
		topLeft->Destory();
		bottomRight->Destory();
		bottomLeft->Destory();

		divded = false;

		//delete its own grid
		topRight->activeGrid = false;
		topRight = NULL;

		topLeft->activeGrid = false;
		topLeft = NULL;

		bottomRight->activeGrid = false;
		bottomRight = NULL;

		bottomLeft->activeGrid = false;
		bottomLeft = NULL;
	}
	else //if its the children
	{
		theMesh = NULL;
		objNumber = 0;
		obj = NULL;
	}
}

bool QuadTreeGrid::Insert(EntityBase* obj)
{
	if (!obj)
		return false;

	if (!CheckInGrid(obj)) //if this obj should belong in this grid, dont bother checking
		return false;

	++objNumber;

	if (objNumber <= 1)
	{
		this->obj = obj;
		return true;
	}
	else if (!divded) //if there is already an obj in this grid, and grid not divided yet
		DivideToFour();

	//reinsert the other obj
	if (topLeft->Insert(this->obj))
		this->obj = NULL;
	else if (topRight->Insert(this->obj))
		this->obj = NULL;
	else if (bottomRight->Insert(this->obj))
		this->obj = NULL;
	else if (bottomLeft->Insert(this->obj))
		this->obj = NULL;

	//check where it can insert in
	if (topLeft->Insert(obj))
		return true;
	if (topRight->Insert(obj))
		return true;
	if (bottomRight->Insert(obj))
		return true;
	if (bottomLeft->Insert(obj))
		return true;
}

bool QuadTreeGrid::ReAllocate(EntityBase* obj) //reallocate to parent
{
	if (!obj)
		return false;

	if (this->parentGrid->CheckInGrid(obj)) //check if obj belongs in parent grid
	{
		if (this->parentGrid->Insert(obj)) //try inserting in parent
		{
			--this->parentGrid->objNumber;
			return true;
		}
	}
	else
	{
		this->parentGrid->ReAllocate(obj);
		--this->parentGrid->objNumber; //if cannot fit inside parent anymore, decrease obj count					 
		this->parentGrid->CombineGrids();
	}

	return false;
}

void QuadTreeGrid::DivideToFour()
{
	float halfGridWidth = gridWidth / 2;
	float halfGridHeight = gridHeight / 2;

	topLeft = new QuadTreeGrid(startingPos, halfGridWidth, halfGridHeight, this);
	topLeft->SetMesh(meshName);
	
	topRight = new QuadTreeGrid(Vector3(startingPos.x + halfGridWidth, 0.f, startingPos.z), halfGridWidth, halfGridHeight, this);
	topRight->SetMesh(meshName); 
	
	bottomLeft = new QuadTreeGrid(Vector3(startingPos.x, 0.f, startingPos.z - halfGridHeight), halfGridWidth, halfGridHeight, this);
	bottomLeft->SetMesh(meshName);
	
	bottomRight = new QuadTreeGrid(Vector3(startingPos.x + halfGridWidth, 0.f, startingPos.z - halfGridHeight), halfGridWidth, halfGridHeight, this);
	bottomRight->SetMesh(meshName);

	divded = true;
}

void QuadTreeGrid::CombineGrids()
{
	if (objNumber > 1) //if theres more than 1 object in the grid dont need to combine it
		return;

	if (!divded) //if its not divided its fine, dont need to combine
		return;

	if (topRight->GetObj())
		this->obj = topRight->GetObj();
	else if (topLeft->GetObj())
		this->obj = topLeft->GetObj();
	else if (bottomRight->GetObj())
		this->obj = bottomRight->GetObj();
	else if (bottomLeft->GetObj())
		this->obj = bottomLeft->GetObj();

	Destory();

	divded = false;

	return;
}

void QuadTreeGrid::Update()
{
	if (divded) //if its divided, check its children instead
	{
		if (topRight)
			topRight->Update();
		if (topLeft)
			topLeft->Update();
		if (bottomRight)
			bottomRight->Update();
		if (bottomLeft)
			bottomLeft->Update();
	}
	else //check its own entity
	{
		if (!obj) //if theres no obj in the child, just return
			return;

		if (CheckInGrid(this->obj)) //if its inside grid, dont bother doing anymore things
			return;

		EntityBase* obj = this->obj;
		//deal with the entity, collaspe grid, search through upper levels, if it fits or not
		this->obj = NULL;
		--objNumber; //decrease the obj number in this grid here
		ReAllocate(obj); //reallocate entity to another grid
	}
}

bool QuadTreeGrid::CheckInGrid(EntityBase* obj)
{
	if (obj->GetPosition().x > startingPos.x && obj->GetPosition().x < startingPos.x + gridWidth && obj->GetPosition().z < startingPos.z && obj->GetPosition().z > startingPos.z - gridHeight)
		return true; //its inside grid

	return false;
}

vector<EntityBase*> QuadTreeGrid::FindObjectsInRange(Vector3 position, float range)
{
	vector<EntityBase*> objInRange;

	//check if rectangle is inside or not
	if (position.x - range > startingPos.x + gridWidth || position.x + range < startingPos.x - gridWidth || position.y - range > startingPos.y + gridHeight || position.y + range < startingPos.y - gridHeight)
		return objInRange;
	
	if (divded) //check its children
	{
		vector<EntityBase*> objsFoundInChildren;
		objsFoundInChildren = topRight->FindObjectsInRange(position, range);
		objInRange.insert(objInRange.end(), objsFoundInChildren.begin(), objsFoundInChildren.end()); //append the objs found in children to the original 

		objsFoundInChildren = topLeft->FindObjectsInRange(position, range);
		objInRange.insert(objInRange.end(), objsFoundInChildren.begin(), objsFoundInChildren.end());

		objsFoundInChildren = bottomLeft->FindObjectsInRange(position, range);
		objInRange.insert(objInRange.end(), objsFoundInChildren.begin(), objsFoundInChildren.end());

		objsFoundInChildren = bottomRight->FindObjectsInRange(position, range);
		objInRange.insert(objInRange.end(), objsFoundInChildren.begin(), objsFoundInChildren.end());
	}
	else
	{
		if (!obj) 
			return objInRange;

		if ((position - obj->GetPosition()).LengthSquared() < range * range) //when the object is in range too
			objInRange.push_back(obj); //just push back with the entity found
	}

	return objInRange;
}

void QuadTreeGrid::DeleteObjects(EntityBase* obj)
{
	if (!CheckInGrid(obj)) //if not inside grid dont bother doing anything
		return;

	--objNumber; //reduce the number of objects inside this grid

	if (divded) //find if it exists in children
	{
		topRight->DeleteObjects(obj);
		topLeft->DeleteObjects(obj);
		bottomRight->DeleteObjects(obj);
		bottomLeft->DeleteObjects(obj);
		CombineGrids(); //after finding and delting the objects, check if can combine grids
	}
	else
	{
		this->obj = NULL; //take out the object
	}
}

EntityBase* QuadTreeGrid::GetObj()
{
	if (!obj)
	{
		if (divded)
		{
			if (topLeft->GetObj())
				return topLeft->GetObj();
			else if (topRight->GetObj())
				return topRight->GetObj();
			else if (bottomLeft->GetObj())
				return bottomLeft->GetObj();
			else if (bottomRight->GetObj())
				return bottomRight->GetObj();
		}
		else
			return NULL;
	}

	return obj;
}

bool QuadTreeGrid::GetActive()
{
	return activeGrid;
}

void QuadTreeGrid::SetActive(bool active)
{
	this->activeGrid = active;
}

//---------------------------------------grid rendering
void QuadTreeGrid::SetMesh(const std::string& _meshName)
{
	meshName = _meshName;
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh != nullptr)
	{
		theMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	}
}

void QuadTreeGrid::SetMeshRenderMode(SMeshRenderMode meshRenderMode)
{
	this->meshRenderMode = meshRenderMode;
}

void QuadTreeGrid::Render(void)
{
	if (!divded && obj == NULL) //if theres no obj inside, dont render
		return;

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	if (!divded)
	{
		modelStack.PushMatrix();
			modelStack.Translate(orginPt.x, -9.f, orginPt.z);
			modelStack.Rotate(-90, 1, 0, 0);
			modelStack.Scale(gridWidth, gridHeight, 1.0f );
			RenderHelper::RenderMesh(theMesh);
		modelStack.PopMatrix();
	}
	else //render the tiny quads instead
	{
		topRight->Render();
		topLeft->Render();
		bottomRight->Render();
		bottomLeft->Render();
	}
}

QuadTreeGrid::SMeshRenderMode QuadTreeGrid::GetMeshRenderMode(void) const
{
	return meshRenderMode;
}

