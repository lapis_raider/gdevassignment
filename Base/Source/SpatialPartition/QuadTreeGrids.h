#pragma once

#include "EntityBase.h"
#include "Vector3.h"
#include "Mesh.h"
#include <vector>
using namespace std;

#include "../LevelOfDetails/LevelOfDetails.h"

//Include GLEW
#include <GL/glew.h>

class QuadTreeGrid
{
public:
	enum SMeshRenderMode
	{
		WIRE,
		FILL,
		NUM_MODE
	};

	//--------------for rendering the grid out
	// Set a particular grid's Mesh
	void SetMesh(const std::string& _meshName);
	// Set Mesh's Render Mode
	void SetMeshRenderMode(QuadTreeGrid::SMeshRenderMode meshRenderMode = FILL);
	// Get Mesh's Render Mode
	QuadTreeGrid::SMeshRenderMode GetMeshRenderMode(void) const;
	// Render the grid
	// Set the Level of Detail for objects in this CGrid
	void SetDetailLevel(const CLevelOfDetails::DETAIL_LEVEL theDetailLevel);

	void Render(void);
	void Update(void);
	bool ReAllocate(EntityBase* obj);

	QuadTreeGrid();
	QuadTreeGrid(Vector3 startingPos, float gridWidth, float gridHeight, QuadTreeGrid* parent = NULL);
	~QuadTreeGrid();

	void Init(Vector3 startingPos, float gridWidth, float gridHeight, QuadTreeGrid* parent = NULL);
	bool Insert(EntityBase*);
	void DivideToFour();
	void CombineGrids();
	bool CheckInGrid(EntityBase*);
	void Destory();
	bool GetActive();
	void SetActive(bool);
	vector<EntityBase*> FindObjectsInRange(Vector3 position, float range);
	void DeleteObjects(EntityBase* obj);

	EntityBase* GetObj();

private:
	EntityBase* obj; //the obj in the grid
	bool divded; //check if the quad is already divided into 4

	Vector3 startingPos; //the top left corner to calculate other quads pos
	Vector3 orginPt; //the centre pt
	float gridWidth;
	float gridHeight;

	QuadTreeGrid* parentGrid;
	QuadTreeGrid* topRight;
	QuadTreeGrid* topLeft;
	QuadTreeGrid* bottomRight;
	QuadTreeGrid* bottomLeft;

	int objNumber;
	bool activeGrid;

	// The mesh to represent the grid
	Mesh* theMesh;
	// Define the mesh render mode
	SMeshRenderMode meshRenderMode;
	std::string meshName;

};