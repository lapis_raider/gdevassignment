#pragma once

#include "Vector3.h"
#include "QuadTreeGrids.h"
#include "EntityBase.h"
#include "SingletonTemplate.h"
#include "../FPSCamera.h"

class QuadTree : public Singleton<QuadTree>
{
	friend Singleton<QuadTree>;

	// Constructor
	QuadTree(void);

	Vector3 startingPos;
	Vector3 origin;
	float width;
	float height;

	QuadTreeGrid* root; //this is the root grid

	std::string _meshName; // Name of the mesh
// Define the mesh render mode
	QuadTreeGrid::SMeshRenderMode meshRenderMode;
	// LOD distances
	float LevelOfDetails_Distances[2];


public:

	// Destructor
	virtual ~QuadTree();

	// Destroy the Singleton instance
	void Destroy();

	//get the width and height of the grid
	void Init(const Vector3 originPos,
		const float gridWidth, const float gridHeight);

	// Add a new object
	void Add(EntityBase* theObject);

	void Update();

	vector<EntityBase*> ObjsInRange(Vector3 dist, float range);
	void Remove(EntityBase* obj);

	//----------for rendering
	// Set Mesh's Render Mode
	void SetMeshRenderMode(QuadTreeGrid::SMeshRenderMode meshRenderMode);
	// Get Mesh's Render Mode
	QuadTreeGrid::SMeshRenderMode GetMeshRenderMode(void) const;

	// Set a particular grid's Mesh
	void SetMesh(const std::string& _meshName);

	// Render the spatial partition
	void Render(Vector3* theCameraPosition = NULL, Vector3* theCameraDirection = NULL);

	// Set LOD distances
	void SetLevelOfDetails(const float distance_High2Mid, const float distance_Mid2Low);
	// Check if a CGrid is visible to the camera
	bool IsVisible(Vector3 theCameraPosition, Vector3 theCameraDirection, const int xIndex, const int zIndex);
};